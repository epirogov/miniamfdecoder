﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniAmfDecoder
{
    public class DecoderService : IDecoderService
    {
        private readonly byte[] HEADER_VERSION = new byte[] { 0x00, 0xbf };
        private readonly byte[] HEADER_SIGNATURE;
        private readonly byte PADDING_BYTE = 0x00;

        public DecoderService()
        {
            var enc = Encoding.GetEncoding(1252);
            HEADER_SIGNATURE = enc.GetBytes("TCSO").Concat(new byte[] { 0x00, 0x04, 0x00, 0x00, 0x00, 0x00 }).ToArray();
        }

        public void Decode(Stream stream, bool strict = false)
        {
            var reader = new BinaryReader(stream);

            var version = reader.ReadBytes(2);

            if (!HEADER_VERSION.SequenceEqual(version))
            {
                throw new FormatException("Unknown SOL version in header");
            }

            var length = reader.ReadInt64();

            if (strict && reader.BaseStream.Length - reader.BaseStream.Position != length)
            {
                throw new FormatException("Inconsistent stream header length");
            }

            var signature = reader.ReadBytes(10);

            if (!HEADER_SIGNATURE.SequenceEqual(signature))
            {
                throw new FormatException("Invalid signature");
            }

            length = reader.ReadInt16();
            var rootName = Encoding.UTF8.GetString(reader.ReadBytes((ushort)length));

            var padding = new byte[] { PADDING_BYTE, PADDING_BYTE, PADDING_BYTE };

            if (!padding.SequenceEqual(reader.ReadBytes(3)))
            {
                throw new FormatException("Invalid padding read");
            }


        }


    }
}

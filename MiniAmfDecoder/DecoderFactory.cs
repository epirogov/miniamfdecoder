﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiniAmfDecoder
{
    public class DecoderFactory
    {
        private const byte AMF0 = 0;
        private const byte AMF3 = 3;
        private static readonly byte[] ENCODING_TYPES = new byte[] { AMF0, AMF3 };
        private const byte DEFAULT_ENCODING = AMF3;

        public AmfDecoder GetDecoder(byte enc, string args, string[] kwargs)
        {
            bool useExt = kwargs.Contains("use_ext");
            var module = GetAmfModule(enc, useExt);
            return module.Decoder(args, kwargs);
        }

        internal static IEnumerator<Amf0Module> GetAmfModule(byte enc, bool? useExt)
        {
            if (!ENCODING_TYPES.Contains(enc))
            {
                throw new ArgumentException(string.Format("Invalid AMF version: {0} specified", enc));
            }

            var module_name = string.Format(".amf{0}", enc);

            string[] packages;
            if (!useExt.HasValue)
            {
                packages = new string[] { "miniamf._accel", "miniamf" };
            }
            else if (useExt.Value)
            {
                packages = new string[] { "miniamf._accel" };
            }
            else
            {
                packages = new string[] { "miniamf" };
            }

            foreach (var pkg in packages)
            {
                yield ImportModule(module_name, pkg);
            }
        }

        private static void ImportModule(string module_name, string pkg)
        {
            throw new NotImplementedException();
        }

        internal AmfDecoder Decoder(string args, string[] kwargs)
        {
            throw new NotImplementedException();
        }
    }
}

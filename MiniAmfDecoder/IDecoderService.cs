﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniAmfDecoder
{
    interface IDecoderService
    {
        void Decode(Stream stream, bool strict = false);
    }
}

We need Python code ported to C#.

This is for a open-source project (https://github.com/zackw/mini-amf/tree/master/miniamf) that parses a Adobe AMF3 Stream.

The functionality of SOL.py's Decode need only be implemented. 

Sample input files that work with SOL.py will be provided. The C# version must output equivalent results.

---------
Bytes literals are always prefixed with 'b' or 'B'; they produce an instance of the bytes type instead of the str type. They may only contain ASCII characters; bytes with a numeric value of 128 or greater must be expressed with escapes.

---------
BufferedStream Class 

The BufferedStream class also extends the Stream class. Buffers, or cached blocks of data in memory, provide speed and stability to the process of reading or writing because they prevent numerous calls to the operating system. Buffered streams are used in conjunction with other streams to provide better read/write performance. The BufferedStream class can be used to either read data or write data but it cannot be used to perform both read and write operations together. The class has been optimized so that it maintains a suitable buffer at all times. When a buffer is not required, instead of slowing down the process, the class does not allocate any space in memory. File streams are already buffered and therefore a buffered stream is generally used to buffer network streams used in networking applications. 

-------------------
How do I know current offset of BinaryReader in C#?

You can obtain the underlying stream by

var stream = reader.BaseStream;
and get the position by

stream.Position
--------------------------
C# UNICODE to ANSI conversion

System.Text.Encoding ansi1252 = System.Text.Encoding.GetEncoding(1252);
System.Text.Encoding ansi1253 = System.Text.Encoding.GetEncoding(1253);
---------------------------
Comparing two byte arrays in .NET

using System;
using System.Linq;
...
var a1 = new int[] { 1, 2, 3};
var a2 = new int[] { 1, 2, 3};
var a3 = new int[] { 1, 2, 4};
var x = a1.SequenceEqual(a2); // true
var y = a1.SequenceEqual(a3); // false

C# Build hexadecimal notation string

Byte value = 0x0FF;
int value = 0x1B;
So, its easily possible to pass an hexadecimal literal into your string:

string foo = String.Format("{0} hex test", 0x0BB);

Please try to avoid the \x escape sequence. It's difficult to read because where it stops depends on the data. For instance, how much difference is there at a glance between these two strings?

"\x9Good compiler"
"\x9Bad compiler"
In the former, the "\x9" is tab - the escape sequence stops there because 'G' is not a valid hex character. In the second string, "\x9Bad" is all an escape sequence, leaving you with some random Unicode character and " compiler".

I suggest you use the \u escape sequence instead:

"\u0009Good compiler"
"\u0009Bad compiler"

-----------------------

Python - multiple %s string

TEXT = 'Hi, your first name is %s and your last name is %s' % (Fname, Lname)

-----------------------
Initialize list with same bool value

>>> [False] * 10
[False, False, False, False, False, False, False, False, False, False]

-----------------------

Pure Python Mode
In some cases, it’s desirable to speed up Python code without losing the ability to run it with the Python interpreter. While pure Python scripts can be compiled with Cython, it usually results only in a speed gain of about 20%-50%.

To go beyond that, Cython provides language constructs to add static typing and cythonic functionalities to a Python module to make it run much faster when compiled, while still allowing it to be interpreted. This is accomplished either via an augmenting .pxd file, or via special functions and decorators available after importing the magic cython module.

Although it is not typically recommended over writing straight Cython code in a .pyx file, there are legitimate reasons to do this - easier testing, collaboration with pure Python developers, etc. In pure mode, you are more or less restricted to code that can be expressed (or at least emulated) in Python, plus static type declarations. Anything beyond that can only be done in .pyx files with extended language syntax, because it depends on features of the Cython compiler.



Unread links
https://en.wikipedia.org/wiki/Action_Message_Format
https://stackoverflow.com/questions/10675054/how-to-import-a-module-in-python-with-importlib-import-module
http://cython.readthedocs.io/en/latest/src/tutorial/pure.html
